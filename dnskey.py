#!/usr/bin/python3

from sys import argv
from eddsa2 import Ed25519, Ed448, eddsa_obj
from socket import htons,htonl
from struct import *
import base64, hashlib, binascii

algorithms = { 'Ed25519': 15, 'Ed448': 16 }

tbd = { 15:'15', 16:'16' }

examples = [
    (b'ODIyNjAzODQ2MjgwODAxMjI2NDUxOTAyMDQxNDIyNjI=', 'Ed25519'),
    (b'DSSF3o0s0f+ElWzj9E/Osxw8hLpk55chkmx0LYN5WiY=', 'Ed25519'),
    (b'xZ+5Cgm463xugtkY5B0Jx6erFTXp13rYegst0qRtNsOYnaVpMx0Z/c5EiA9x8wWbDDct/U3FhYWA', 'Ed448'),
    (b'WEykD3ht3MHkU8iH4uVOLz8JLwtRBSqiBoM6fF72+Mrp/u5gjxuB1DV6NnPO2BlZdz4hdSTkOdOA', 'Ed448')
]

rrclass = { "IN": 0x0001, "CH": 0x0003 }
rrtype = { "A": 0x0001, "MX": 0x000f }

ttl = 3600
flags = 257

old_name = None

#rtype = "A"
#rdata = pack("BBBB", 192, 0, 2, 1)

def wire_format(name):
    wf = b''
    foo = name.split(u'.')
    for label in foo:
        wf += bytearray(chr(len(label)) + label, 'us-ascii')
    return bytearray(wf)

origin = "example.com."
owner = "example.com."

mx_priority = 10
mx_target = "mail.example.com."

rtype = "MX"
rdata = pack("H", htons(mx_priority)) + wire_format(mx_target)

rr_1 = wire_format(owner)
rr_1 += pack("HHIH", htons(rrtype[rtype]), htons(rrclass["IN"]), htonl(ttl), htons(len(rdata)))
rr_1 += rdata

expiration = 1440021600
inception  = 1438207200

protocol = 3

def dnskey_rdata(flags, protocol, algorithm, public_key):
    return pack("HBB", htons(flags), protocol, algorithm) + bytearray(public_key)

def keytag(flags, protocol, algorithm, public_key):
    rdata = dnskey_rdata(flags, protocol, algorithm, public_key)
    ac = 0
    for i in range(len(rdata)):
        if (i & 1):
            ac += rdata[i]
        else:
            ac += rdata[i] << 8
        ac = ac & 0xFFFFFFFF

    ac += (ac >> 16) & 0xFFFF
    return ac & 0xFFFF

def labels(name):
    return len(name.split("."))-1

def split_at(s, lpad):
    div = 1
    while True:
        step = len(s) / div
        while ((step * div) < len(s)):
            step += 1
        if step < (72-len(owner)-1):
            break
        div += 1
    return int(step)

def print_wrap(msg, lpad, wrap_at, end_with):
    step = wrap_at - lpad
#    step = split_at(signature64, len(owner)+1)
    start = 0
    end_s = ""
    while start < len(msg):
        if (start + step) >= len(msg):
            end_s = end_with
        print("%s%s%s" % ("".ljust(lpad), msg[start:start+step].decode("us-ascii"), end_s))
        start += step

for (secret,name) in examples:
    eddsa = eddsa_obj(name)
    private_key,public_key = eddsa.keygen(base64.b64decode(secret))

    algo = algorithms[name]
    
    key_tag = keytag(flags, protocol, algo, public_key)

    rrsig_data = pack("HBBIIIH", htons(rrtype[rtype]), algo, labels(owner), htonl(ttl), htonl(expiration), htonl(inception), htons(key_tag)) + wire_format(origin)

    signature_data = rrsig_data + rr_1

    signature = eddsa.sign(private_key, public_key, signature_data)
    signature64 = base64.b64encode(signature)

    if old_name != name:
        if old_name != None:
            print("      </section>")
        print("      <section title=\"%s Examples\">" % (name))
        old_name = name
    
    print("        <figure align=\"center\">")
    print("          <artwork align=\"left\"><![CDATA[")
    print("Private-key-format: v1.2")
    print("Algorithm: %s (%s)" % (tbd[algo], name.upper()))
    print("PrivateKey: %s" % (secret.decode("us-ascii")))
    print("")    
    print("%s %s %s %s %s %s %s (" % (owner, ttl, "IN", "DNSKEY", flags, protocol, tbd[algo]))
    print_wrap(base64.b64encode(public_key), len(owner)+1, 72, " )")
    print("")
    print("%s %s %s %s %s %s %s (" % (owner, ttl, "IN", "DS", key_tag, tbd[algo], 2))
    print_wrap(binascii.hexlify(hashlib.sha256(wire_format(owner) + dnskey_rdata(flags, protocol, algo, public_key)).digest()), len(owner)+1, 72, " )")
    print("")
    print("%s %s %s %s %s %s" % (owner, ttl, "IN", rtype, mx_priority, mx_target))
    print("")
    print("%s %s %s %s %s %s %s %s (" % (owner, ttl, "IN", "RRSIG", rtype, tbd[algo], labels(owner), ttl))
    print("%s %s %s %s %s (" % ("".ljust(len(owner)), expiration, inception, key_tag, origin))
    print_wrap(signature64, len(owner)+1, 72, " )")    
    print("         ]]></artwork>")
    print("        </figure>")

print("      </section>")
